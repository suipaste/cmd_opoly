/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cmdmonopoly;

/**
 *
 * @author stephen
 */
public class Dice {

    private int die1;   // Number showing on the first die.
    private int die2;   // Number showing on the second die.
    private int total;
    private boolean isDouble;

    public boolean isIsDouble() {
        return isDouble;
    }

    public void setIsDouble(boolean isDouble) {
        this.isDouble = isDouble;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getDie1() {
        return die1;
    }

    public void setDie1(int die1) {
        this.die1 = die1;
    }

    public int getDie2() {
        return die2;
    }

    public void setDie2(int die2) {
        this.die2 = die2;
    }

    public void roll() {
        // Roll the dice by setting each of the dice to be
        // a random number between 1 and 6.
        this.setDie1((int) (Math.random() * 6) + 1);
        this.setDie2((int) (Math.random() * 6) + 1);
        
        this.setTotal(this.getDie1() + this.getDie2());
        
        if (this.getDie1() == this.getDie2())
        {
            this.setIsDouble(true);
        } else
        {
            this.setIsDouble(false);
        }
    }
}
