/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cmdmonopoly;

/**
 *
 * @author stephen
 */
public class Tax extends Space{
    int taxValue;

    public int getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(int taxValue) {
        this.taxValue = taxValue;
    }

    public Tax(int taxValue, String name) {
        super(name);
        this.taxValue = taxValue;
    }
    
    @Override
    public void landOn(Player pPlayer) {
        pPlayer.setMoney(pPlayer.getMoney() - this.getTaxValue());
        System.out.println(pPlayer.getName() + " landed on " + this.getName() + " and must pay tax to the value of £" + this.getTaxValue());
        System.out.println(pPlayer.getName() + " now has £" + pPlayer.getMoney());
    }
    
}
