/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cmdmonopoly;

/**
 *
 * @author stephen
 */
public class GoToJail extends Space{

    public GoToJail(String name) {
        super(name);
    }

    @Override
    public void landOn(Player pPlayer) {
        super.landOn(pPlayer);
        System.out.println("GO TO JAIL! Go directly to Jail, do not pass Go!, do not collect £200.");
        pPlayer.setInJail(true);
    }
    
    
}
