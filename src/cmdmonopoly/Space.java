/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cmdmonopoly;

/**
 *
 * @author stephen
 */
public class Space {
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    /**
    * Returns the name of the Space
    * <p>
    * Returns the name of the space on the board, for example 'Go' or 'Pall Mall'. 
    *
    * @return name
    */
    public String getName() {
        return name;
    }

    public Space(String name) {
        this.name = name;
    }
    
    public void landOn(Player pPlayer) {
        System.out.println(pPlayer.getName() + " landed on " + this.getName());
    }
}
