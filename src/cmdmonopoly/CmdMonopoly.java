/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cmdmonopoly;

import java.util.Scanner;

/**
 *
 * @author stephen
 */
public class CmdMonopoly {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Board monopolyBoard = new Board();
        Dice dice = new Dice();
        Player player1 = new Player("Tophat");
        boolean gameOver = false;
        int turnNumber = 1;

        while (gameOver == false) {
            System.out.println("Turn number " + turnNumber);
            processTurn(monopolyBoard, player1, dice);
            turnNumber++;
        }
    }
    
    private static void pressAnyKeyToContinue()
 { 
        System.out.println("--------------Press enter to continue------------------");
        try
        {
            System.in.read();
        }  
        catch(Exception e)
        {}  
 }

    private static void processTurn(Board pMonopolyBoard, Player pPlayer, Dice pDice) {
        Scanner sc = new Scanner(System.in);        
        Turn turn = new Turn();
       
        String action;

        System.out.println("\n" + pPlayer.getName() + ", it's your turn.");
        System.out.println("You are currently landed on " + pMonopolyBoard.getSpace(pPlayer.getLocation()).getName() + ".");
        System.out.println("You have £" + pPlayer.getMoney() + ".");

        while (turn.isActionComplete() == false) {
            // third double?
            processDoubles(pPlayer, turn, pMonopolyBoard);

            if (turn.isTurnComplete() == false) {
                // In jail?
                processInJail(pPlayer);
                processAvailableActions(pPlayer);


                action = sc.nextLine();
                switch (action) {
                    case "r":
                        movementRoll(pMonopolyBoard, pPlayer, pDice);
                        turn.setActionComplete(true);
                        break;
                    case "p":
                        if (pPlayer.isInJail() && pPlayer.getTurnsInJail() != 3) {
                            if (pPlayer.getMoney() > 50) {
                                pPlayer.setMoney(pPlayer.getMoney() - 50);
                                pPlayer.setInJail(false);

                                movementRoll(pMonopolyBoard, pPlayer, pDice);
                                turn.setActionComplete(false);
                                break;
                            } else {
                                if (pPlayer.getMoney() < 50) {
                                    System.out.println("You don't have enough money");
                                }
                                System.out.println("Invalid action");
                                turn.setActionComplete(false);
                                break;
                            }
                        }
                    default:
                        System.out.println("Invalid action");
                        turn.setActionComplete(false);
                        break;
                }

                if (pDice.isIsDouble()) {
                    turn.setActionComplete(false);
                    turn.setDoubleCount(turn.getDoubleCount()+1);
                }
            }
        }
        System.out.println("\nTurn Complete");
        pressAnyKeyToContinue();
    }

    private static void processAvailableActions(Player pPlayer) {
        System.out.println("\nAvailable actions:");
        System.out.println("* (r)oll dice");
        if (pPlayer.isInJail()) {
            System.out.println("* (p)ay £50 to get out of jail");
        }
    }

    private static void processInJail(Player pPlayer) {
        if (pPlayer.isInJail()) {
            pPlayer.setTurnsInJail(pPlayer.getTurnsInJail() + 1);
            switch (pPlayer.getTurnsInJail()) {
                case 1:
                    System.out.println("This is your 1st turn in jail");
                    break;
                case 2:
                    System.out.println("This is your 2nd turn in jail");
                    break;
                case 3:
                    System.out.println("This is your 3rd turn in jail, you must get out on this turn");
                    break;
            }
        }
    }
    
    private static void processDoubles(Player pPlayer, Turn pTurn, Board pMonopolyBoard) {
        switch (pTurn.getDoubleCount()) {
                case 1:
                    System.out.println("You threw a double, this is your 1st. Have another turn");
                    break;
                case 2:
                    System.out.println("You threw a double, this is your 2nd. Have another turn");
                    break;
                case 3:
                    System.out.println("This is your 3rd double. ");
                    pPlayer.setInJail(true);
                    pTurn.setActionComplete(true);
                    pTurn.setTurnComplete(true);
                    break;
            }
    }

    private static void movementRoll(Board pMonopolyBoard, Player pPlayer, Dice pDice) {
        pDice.roll();
        System.out.println();

        // check for double
        if (pDice.getDie1() == pDice.getDie2()) {
            System.out.println(pPlayer.getName() + " rolled " + pDice.getDie1() + " + " + pDice.getDie2() + " = " + pDice.getTotal() + " which is a double.");
        } else {
            System.out.println(pPlayer.getName() + " rolled " + pDice.getDie1() + " + " + pDice.getDie2() + " = " + pDice.getTotal());
        }

        // deal with jail roll
        if (pPlayer.isInJail()) {
            if (pDice.isIsDouble()) {
                pPlayer.setInJail(false);
                System.out.println("You are no longer in jail");
            }

            if (pPlayer.getTurnsInJail() == 3) {
                System.out.println("You failed to get a double on your 3rd turn in jail, you must pay £50 to get out");
                pPlayer.setInJail(false);
                pPlayer.setMoney(pPlayer.getMoney() - 50);
            }
        }

        if (pPlayer.isInJail() == false) {
            if ((pPlayer.getLocation() + pDice.getTotal()) > 39) {
                pPlayer.setLocation(pPlayer.getLocation() + pDice.getTotal() - 40);
                pPlayer.setMoney(pPlayer.getMoney() + 200);
                System.out.println(pPlayer.getName() + " passed " + pMonopolyBoard.getSpace(0).getName() + " and collects £200");
            } else {
                pPlayer.setLocation(pPlayer.getLocation() + pDice.getTotal());
            }
            pMonopolyBoard.getSpace(pPlayer.getLocation()).landOn(pPlayer);
        }
    }
}
