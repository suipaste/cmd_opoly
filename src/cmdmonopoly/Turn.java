/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cmdmonopoly;

/**
 *
 * @author stephen
 */
public class Turn {

    boolean turnComplete;

    private int doubleCount;
    
    boolean actionComplete;

    public int getDoubleCount() {
        return doubleCount;
    }

    public void setDoubleCount(int doubleCount) {
        this.doubleCount = doubleCount;
    }

    public boolean isTurnComplete() {
        return turnComplete;
    }

    public void setTurnComplete(boolean turnComplete) {
        this.turnComplete = turnComplete;
    }

    public boolean isActionComplete() {
        return actionComplete;
    }

    public void setActionComplete(boolean actionComplete) {
        this.actionComplete = actionComplete;
    }

    public Turn() {
        this.turnComplete = false;
        this.actionComplete = false;
        this.doubleCount = 0;
    }

}
