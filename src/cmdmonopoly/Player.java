/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cmdmonopoly;

import java.util.ArrayList;

/**
 *
 * @author stephen
 */
public class Player {
    private String name;
    private int money;
    private int location;
    private boolean inJail;
    private int turnsInJail;

    public int getTurnsInJail() {
        return turnsInJail;
    }

    public void setTurnsInJail(int turnsInJail) {
        this.turnsInJail = turnsInJail;
    }

    public boolean isInJail() {
        return inJail;
    }

    public void setInJail(boolean inJail) {
        this.inJail = inJail;
        
        if (isInJail() == true)
        {
            // Player is sent to jail
            System.out.println("GO TO JAIL! Go directly to jail. Do not pass GO!, do not collect £200");
            this.setLocation(10);
            
        } else{
            // Player is released from jail
            this.setTurnsInJail(0);
        }
    }
    private ArrayList<String> singleAddress = new ArrayList<String>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public ArrayList<String> getSingleAddress() {
        return singleAddress;
    }

    public void setSingleAddress(ArrayList<String> singleAddress) {
        this.singleAddress = singleAddress;
    }

    public Player(String name) {
        this.name = name;
        this.money = 1500;
        this.location = 0;
        this.inJail = false;
    }
    
    
    
    
}
