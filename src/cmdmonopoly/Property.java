/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cmdmonopoly;

import java.util.Scanner;

/**
 *
 * @author stephen
 */
public abstract class Property extends Space {
    
    private int price;
    private boolean morgaged;
    private Player owner;

    public Player getOwner() {
        return owner;
    }

    public void setOwner(Player owner) {
        this.owner = owner;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isMorgaged() {
        return morgaged;
    }

    public void setMorgaged(boolean morgaged) {
        this.morgaged = morgaged;
    }

    public Property(int price, String name) {
        super(name);
        this.price = price;
        this.morgaged = false;
        this.owner = null;
    }
    
    
    
    @Override
    public void landOn(Player pPlayer) {
        super.landOn(pPlayer);
        
        System.out.println();
                
        Scanner sc = new Scanner(System.in); 
        String action;
        // Who owns the property?
        if (this.getOwner() == null)
        {
            // it hasn't been bought yet and is for sale
            System.out.println("This property is for sale and costs £" + this.getPrice());
            
            // Does the player have enough money to buy it?
            if (pPlayer.getMoney() >= this.getPrice())
            {
                System.out.println("Would you like to purchase " + this.getName() + "?");
                System.out.println("* (y)es");
                System.out.println("* (n)o");
                
                action = sc.nextLine();
                switch (action) {
                    case "y":
                        pPlayer.setMoney(pPlayer.getMoney() - this.getPrice());
                        this.setOwner(pPlayer);
                        System.out.println("You now own " + this.getName());
                        System.out.println();
                        break;
                    case "n":
                        // TODO up for auction
                    default:
                        System.out.println("Invalid action");
                        // TODO action incomplete loop
                        break;
                }
                        
            }
        }
        else if (this.getOwner() == pPlayer) {
            // You own this property
            System.out.println("You own this property");
        }
        else {
            System.out.println("This property is owned by " + this.getOwner().getName());
        }
    }
}
