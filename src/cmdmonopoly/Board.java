/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cmdmonopoly;

/**
 *
 * @author stephen
 */
public class Board {
    
   private Space spaceList[] = {
       new Space("Go!"), // 0
       
       new GroupProperty(2, 60, "Old Kent Road"),
       new Space("Community Chest"),
       new GroupProperty(2, 60, "Whitechapel Road"),
       new Tax(200, "Income Tax"),
       new Space("Kings Cross Station"),
       new GroupProperty(6, 100, "The Angel, Islington"),
       new Space("Chance"),
       new GroupProperty(6, 100, "Euston Road"),
       new GroupProperty(8, 120, "Pentonville Road"),
       
       new Space("Jail"), // 10
       
       new GroupProperty(10, 140, "Pall Mall"),
       new Space("Electric Company"),
       new GroupProperty(10, 140, "Whitehall"),
       new GroupProperty(12, 160, "Northumberland Avenue"),
       new Space("Marylebone Station"),
       new GroupProperty(14, 180, "Bow Street"),
       new Space("Community Chest"),
       new GroupProperty(14, 180, "Marlbrough Street"),
       new GroupProperty(16, 200, "Vine Street"),
       
       new Space("Free Parking"), // 20
       
       new GroupProperty(18, 220, "Strand"),
       new Space("Chance"),
       new GroupProperty(18, 220, "Fleet Street"),
       new GroupProperty(20, 240, "Trafalgar Square"),
       new Space("Fenchurch Street Station"),
       new GroupProperty(22, 260, "Leicester Square"),
       new GroupProperty(22, 260, "Coventry Street"),
       new Space("Water Works"),
       new GroupProperty(24, 280, "Piccadilly"),
       
       new Space("Go to Jail"), // 30
       
       new GroupProperty(26, 300, "Regent Street"),
       new GroupProperty(26, 300, "Oxford Street"),
       new Space("Community Chest"),
       new GroupProperty(28, 320, "Bond Street"),
       new Space("Liverpool Street Station"),
       new Space("Chance"),
       new GroupProperty(35, 350, "Park Lane"),
       new Tax(100, "Super Tax"),
       new GroupProperty(50, 400, "Mayfair") // 39
   }; 

    public Space[] getSpaceList() {
        return spaceList;
    }
    
    public Space getSpace(int pPosition) {
        return spaceList[pPosition];
    }
}
