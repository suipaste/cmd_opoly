/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cmdmonopoly;

/**
 *
 * @author Stephen
 */
public class GroupProperty extends Property {

    private int houses;
    private int hotels;
    private int rent;

    public int getRent() {
        return rent;
    }

    public void setRent(int rent) {
        this.rent = rent;
    }

    public int getHouses() {
        return houses;
    }

    public void setHouses(int houses) {
        this.houses = houses;
    }

    public int getHotels() {
        return hotels;
    }

    public void setHotels(int hotels) {
        this.hotels = hotels;
    }

    /**
     * Create a new coloured group property.
     *
     * @param price The base cost to buy the property
     * @param morgaged Is the property morgaged?
     * @param name The name of the property
     * @param rent The base rent of the property with no houses or hotels
     */
    public GroupProperty(int rent, int price, String name) {
        super(price, name);
        this.rent = rent;
        this.houses = 0;
        this.hotels = 0;
    }

}
